import os
from dotenv import load_dotenv
from yoyo import read_migrations, get_backend
from yoyo.backends import DatabaseBackend
from yoyo.migrations import MigrationList


if __name__ == "__main__":
    load_dotenv()

    # Соединение с базой. Внутри  видно, что для PostgreSQL необходим драйвер psycopg2!
    b: DatabaseBackend = get_backend(os.environ['POSTGRES_DSN'])

    # Все миграции, но выполняться могут и не все, зависит от версии в базе данных.
    migrations: MigrationList = read_migrations('./migrations')

    # Применяет только те миграции, которые должны быть применены к подключенной базе (проверяется таблица с версией)
    b.apply_migrations(b.to_apply(migrations))